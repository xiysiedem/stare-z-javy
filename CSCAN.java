package lab2;

import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;

public class CSCAN {
	static int glowica;
	public static void doIt(Zadanie[] tab, int pozycjaGlowicy) {
		LinkedList<Zadanie> kolejka = new LinkedList<Zadanie>();
		int i = 0, czas = 0,kierunek = 1;
		glowica = pozycjaGlowicy;
		Wynik wynik = new Wynik(0, 0);
		PriorityQueue<Zadanie> kolejkaDeadline = new PriorityQueue<Zadanie>(1,
				new Comparator<Zadanie>() {
					@Override
					public int compare(Zadanie o1, Zadanie o2) {
						int z1 = Math.abs(o1.miejsce - glowica);
						int z2 = Math.abs(o2.miejsce - glowica);
						return z1 < z2 ? -1 : z1 > z2 ? 1 : 0;
					}
				});
		while (i < tab.length || !kolejka.isEmpty()) {
			while (i < tab.length && czas == tab[i].czasZgloszenia) {
				if (tab[i].czyDeadline)
					kolejkaDeadline.add(tab[i++]);
				else
					kolejka.add(tab[i++]);
			}
			if(!kolejkaDeadline.isEmpty()){
				if(Math.abs(kolejkaDeadline.peek().miejsce-glowica)+czas>kolejkaDeadline.peek().deadline){
					wynik.opuszczoneDeadliny++;
					kolejkaDeadline.poll();
					continue;
				}
				Zadanie z = kolejkaDeadline.peek();
				if (z.miejsce > glowica) {
					glowica++;
					wynik.drogaGlowicy++;
				} else if (z.miejsce < glowica) {
					glowica--;
					wynik.drogaGlowicy++;
				}
				if (z.miejsce == glowica) {
					kolejkaDeadline.remove();
				}
			}
			else if (!kolejka.isEmpty()) {
				Iterator<Zadanie> it = kolejka.iterator();
				while(it.hasNext()){
					Zadanie z = it.next();
					if(z.miejsce==glowica) it.remove();
					}
				
				if(glowica==Main.iloscCylindrow) {
					glowica=0;
					wynik.drogaGlowicy+=Main.iloscCylindrow;
				} else{
					glowica+=kierunek;
					wynik.drogaGlowicy++;
				}
			}
			czas++;
		}
		Main.glowicaCSCAN.drogaGlowicy+=wynik.drogaGlowicy;
		Main.glowicaCSCAN.opuszczoneDeadliny+=wynik.opuszczoneDeadliny;
		
	}
}
