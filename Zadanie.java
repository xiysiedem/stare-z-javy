package lab2;

public class Zadanie implements Cloneable{
	int czasZgloszenia,miejsce,deadline;
	boolean czyDeadline;
	public Zadanie(int czasZgloszenia, int miejsce,
			boolean czyDeadline, int deadline) {
		super();
		this.czasZgloszenia = czasZgloszenia;
		this.miejsce = miejsce;
		this.deadline = deadline;
		this.czyDeadline = czyDeadline;
	}
	@Override
	public String toString() {
		return "Zadanie [czasZgloszenia=" + czasZgloszenia + ", miejsce="
				+ miejsce + ", deadline=" + deadline + ", czyDeadline="
				+ czyDeadline + "]";
	}
	@Override
	protected 
	Zadanie clone(){
		return new Zadanie(czasZgloszenia, miejsce, czyDeadline, deadline);
		
	}
}
