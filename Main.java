package lab2;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

public class Main {
	static int iloscCiagowTestowych = 1000, iloscZgloszen = 200,
			iloscCylindrow = 1000, maxOpoznienie = 0, pozycjaGlowicy = 0,
			x = 0;
	static double iloscDeadlinow = 0.2, zasiegDeadlinow = 0.7;
	public static Wynik glowicaFCFS = new Wynik(0, 0), glowicaSSTF = new Wynik(
			0, 0), glowicaSCAN = new Wynik(0, 0),
			glowicaCSCAN = new Wynik(0, 0);

	public static void main(String[] args) {
		for (int i = 0; i < iloscCiagowTestowych; i++) {
			Zadanie[] zad = generujZadania();
			for (int j = 0; j < zad.length; j++) {
				if (zad[j].czyDeadline)
					x++;
			}
			FCFS.doIt(kopiujTablice(zad), pozycjaGlowicy);
			SSTF.doIt(kopiujTablice(zad), pozycjaGlowicy);
			SCAN.doIt(kopiujTablice(zad), pozycjaGlowicy);
			CSCAN.doIt(kopiujTablice(zad), pozycjaGlowicy);
		}
		System.out.println("Srednia ilosc zadan realtime " + x
				/ iloscCiagowTestowych);
		System.out.println("FCFS "
				+ (glowicaFCFS.drogaGlowicy / iloscCiagowTestowych) + " "
				+ glowicaFCFS.opuszczoneDeadliny / iloscCiagowTestowych);
		System.out.println("SSTF "
				+ (glowicaSSTF.drogaGlowicy / iloscCiagowTestowych) + " "
				+ glowicaSSTF.opuszczoneDeadliny / iloscCiagowTestowych);
		System.out.println("SCAN "
				+ (glowicaSCAN.drogaGlowicy / iloscCiagowTestowych) + " "
				+ glowicaSCAN.opuszczoneDeadliny / iloscCiagowTestowych);
		System.out.println("CSCAN "
				+ (glowicaCSCAN.drogaGlowicy / iloscCiagowTestowych) + " "
				+ glowicaCSCAN.opuszczoneDeadliny / iloscCiagowTestowych);
		zapiszDoPliku(
				"dane2.html",
				"<img src=\"https://chart.googleapis.com/chart?chs=320x300&cht=bvs&chds=a&chxt=x,x,x,x,x,x,y&chm=N,000000,0,-1,11&chco=FF0000|00FF00|00AA00|0000FF&chbh=a&chxl=0:|FCFS|SSTF|SCAN|CSCAN|1:||�rednia%20droga%20glowicy|||2:||||||3:||Ilos�%20ci�g�w%20testowych:%20"
						+ iloscCiagowTestowych
						+ ",%20kazdy%20po%20zadan:%20"
						+ iloscZgloszen
						+ "||%20|4:||Rozmiar%20dysku.:%20"
						+ iloscCylindrow
						+ ",%20||%20|5:Max.%20opoznienie:%20"
						+ maxOpoznienie
						+ "%20:%20Ilosc%20watkow%20realtime:%20"
						+ x
						/ iloscCiagowTestowych
						+ "||&chd=t:"
						+ glowicaFCFS.drogaGlowicy
						/ iloscCiagowTestowych
						+ ","
						+ glowicaSSTF.drogaGlowicy
						/ iloscCiagowTestowych
						+ ","
						+ glowicaSCAN.drogaGlowicy
						/ iloscCiagowTestowych
						+ ","
						+ glowicaCSCAN.drogaGlowicy
						/ iloscCiagowTestowych
						+ "\"></img>");

	}

	private static Zadanie[] generujZadania() {
		Zadanie[] tab = new Zadanie[iloscZgloszen];
		Random r = new Random();
		for (int i = 0; i < iloscZgloszen; i++) {
			tab[i] = new Zadanie(r.nextInt(maxOpoznienie + 1),
					r.nextInt(iloscCylindrow + 1),
					r.nextDouble() < iloscDeadlinow, (int) (zasiegDeadlinow * iloscCylindrow));
			tab[i].deadline += tab[i].czasZgloszenia;
		}
		Arrays.sort(tab, new Comparator<Zadanie>() {

			@Override
			public int compare(Zadanie o1, Zadanie o2) {
				return o1.czasZgloszenia > o2.czasZgloszenia ? 1
						: o1.czasZgloszenia < o2.czasZgloszenia ? -1 : 0;
			}
		});
		return tab;
	}

	private static Zadanie[] kopiujTablice(Zadanie[] tab) {
		Zadanie[] wyn = new Zadanie[tab.length];
		for (int i = 0; i < wyn.length; i++) {
			wyn[i] = tab[i].clone();
		}
		return wyn;
	}

	private static void zapiszDoPliku(String plik, String dane) {
		try {
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(plik, true), Charset.forName("UTF-8")));
			bw.write(dane + "\n");
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
